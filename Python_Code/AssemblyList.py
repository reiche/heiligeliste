import pandas
import os
import copy
import ComponentList
import numpy as np
class AssemblyList:
    def __init__(self):
        self.assembly={}
        self.assemblylist = {}
        self.components = ComponentList.ComponentList()
        self.check={}

    def readFiles(self,filedir,folder,phase):
        self.components.readFiles(folder,phase)
        self.assembly.clear()
        self.assemblylist.clear()
        for subdir, dirs, files in os.walk(filedir):
            for file in files:
                filepath = subdir + os.sep + file
                path=subdir.split(os.sep)[-1]
                if 'Phase' in path:
                    print('Ignoring file',file,'for sub-assembly list')
                    continue
                if filepath.endswith(".xlsx"):
                    if not 'Old' in filepath:
                        print('Parsing:',filepath)
                        self.parseFile(filepath)

    def parseFile(self,filepath):
#        ext = filepath.split('.')[-1]
#        print(ext,filepath)
        data = pandas.read_excel(filepath,sheet_name=None)
        for key in data.keys():
            tab = data[key]

            parents = tab[tab['Status'] == 'OK']['Parent-Name'].values.astype(str)
            assembly = tab[tab['Name'].notnull()]
            for parent in parents:
                if not parent in self.assembly:
                    self.assembly[parent]=[]
                for i in range(assembly.shape[0]):
                    if isinstance(tab['Name'][i],str):
                        name = parent[0:7]+tab['Name'][i][6:]
                        soff = tab['S Offset'][i]
                        part3d = tab['3D-Part'][i]
                        type = tab['Type'][i]
                        if name in self.assemblylist.keys():
                            print('*** Error: Duplicated generation of assembly element:',name,'for parent',parent)
                        else:
                            self.assemblylist[name] = 1
                            self.assembly[parent].append({'Name':name,'soff':soff,'3DPart':part3d,'Type':type})

    def generateLayout(self,proto):

        DL=[]
        DLsave=[]
        self.check.clear()
        for pkey in proto.info.keys():
            pele=copy.deepcopy(proto.info[pkey])
            # add new entries for holy list and 3DX list
            pele['3DX_Part']=''
            pele['3DX_Type']=''
            pele['3DX_Section'] = ''
            pele['Description']=''
            pele['Type']=''
            pele['Category']=''
            if len(pele['Suffix']) < 2:
                name = '%s-CELL' % pele['Prefix']
                if len(DLsave)> 0:
                    esec=copy.deepcopy(DLsave[0])
                    sname=esec['Name'][0:7]
                    sec = self.components.find_section(sname)
                    if not sec is None:
                        secOG = sec['3DX-Section OG'].to_string(index=False, na_rep='').strip()
                        if 'ASM' in secOG:
                            esec['3DX-Section']=secOG
                        else:
                            esec['3DX-Section'] = 'ASM-'+sec['3DX-Section OG'].to_string(index=False, na_rep='').strip()
                        esec['Description']=sec['Description'].to_string(index=False,na_rep='').strip()
                    esec['3DX-Part'] = ''
                    esec['Name'] = sname+' OG'
                    DL.append(esec)
                    for ele in DLsave:
                        DL.append(ele)
                    DLsave.clear()
                zeroidx = len(DL)
            else:
                name = '%s-%s%3.3d' % (pele['Prefix'],pele['Suffix'],pele['Index'])
            # find entry in component list
            cinfo = self.components.findProtoElement(name,pele['Baugruppe'])
            # check for changing orientation of dipole
            if 'MBN' in pkey:
                dip = self.components.find_dipole(name)
                if dip is None:
                    align = pele['Alignment']
                else:
                    align = dip['Alignment'].to_string(index=False,na_rep='').strip()
                if not align == pele['Alignment']:
                    if align =='Entrance':
                        ev =pele['EV1'][1:4]
                    elif align == 'Exit':
                        ev = pele['EV2'][1:4]
                    else:
                        ev = 0.5*(pele['EV1'][1:4]+pele['EV2'][1:4])
                    angh = np.arctan2(ev[0], ev[2]) * 180 / np.pi
                    rh = np.sqrt(ev[2] * ev[2] + ev[0] * ev[0])
                    angv = np.arctan2(ev[1], rh) * 180 / np.pi
                    print('Adjustment of Dipole Orientation:',name)
                    print('Protolist    : Alignment:',pele['Alignment'],'ANGLE-H:',pele['Yaw'],'ANGLE-V:',pele['Pitch'])
                    print('HL-Components: Alignment:',align,'ANGLE-H:',angh,'ANGLE-V:',angv)
                    pele['Yaw'] = angh
                    pele['Pitch'] = angv
            if cinfo:
                pele['3DX_Part'] = cinfo['3DX_PART']
                pele['3DX_Type'] = self.components.find3DXType(cinfo['3DX_PART'])
                pele['Description']=cinfo['DESC']
                pele['Type'] = cinfo['CNAME']
                pele['Category']=cinfo['CAT']
            entry = self.generateEntry(name,pele,'','')
            if entry:
                DL.append(entry)
            if name in self.assembly.keys():
                parent = name
                for ass in self.assembly[name]:
                    pasm=copy.deepcopy(pele)
                    name = ass['Name']
                    pasm['Description'] = ''
                    pasm['Type'] = ass['Type']
                    pasm['Category'] = ''
                    if isinstance(ass['3DPart'],str):
                        pasm['3DX_Part'] = ass['3DPart']
                        pasm['3DX_Type'] = self.components.find3DXType(ass['3DPart'])
                    else:
                        pasm['3DX_Part'] = ''
                        pasm['3DX_Type'] = ''
                    pasm['L (m)']=0.
                    ds = ass['soff']
                    ev = pasm['EV2']  # outgoing vector
                    if ds > 0:
                        pasm['PV']=pasm['PV']+ds*pasm['EV2']
                    else:
                        pasm['PV'] = pasm['PV'] + ds * pasm['EV1']
                        ev = pasm['EV1']   # incoming vector
#                    if 'SINBC02' in name:
#                        print(name)
#                        print(pasm['EV1'])
#                        print(pasm['EV2'])
                    angh = np.arctan2(ev[1], ev[3]) * 180 / np.pi
                    rh = np.sqrt(ev[3] * ev[3] + ev[1] * ev[1])
                    angv = np.arctan2(ev[2], rh) * 180 / np.pi
                    pasm['Yaw'] = angh
                    pasm['Pitch'] = angv

                    entry = self.generateEntry(name,pasm,parent,'%f' % ds)
                    if entry:
                        if 'ZERO' in name:
                            DL[zeroidx]=entry
                        else:
                            if 'RKLY' in name:
                                DLsave.append(entry)
                            else:
                                DL.append(entry)
        return DL

    def generateEntry(self,name,data,parent,soff):

        if 'ZERO' in name:
            name=name[0:7]
            sec = self.components.find_section(name)
            if sec is None:
                return
            data['3DX_Section']=sec['3DX-Section UG'].to_string(index=False,na_rep='').strip()
            if 'RKLY' in name:
                data['3DX_Section'] = sec['3DX-Section OG'].to_string(index=False, na_rep='').strip()
            data['Description']=sec['Description'].to_string(index=False,na_rep='').strip()


        if name in self.check.keys():
            print('*** Error: Duplicate Element: %s (Parent: %s, Parent of existing entry: %s)' % (name,parent,self.check[name]))
            return None
        else:
            self.check[name]=parent
        ret={}
        ret['Name'] = name
        ret['Type'] = data['Type']
        if 'known' in data['Type']:
            print(name,'unknow type')
        ret['S [m]']=data['PV'][0]
        ret['Length [mm]'] = data['L (m)']*1e3
        ret['Xv [mm]']=data['PV'][1]*1e3
        ret['Yv [mm]'] = data['PV'][2]*1e3-1200.
        ret['Zv [mm]'] = data['PV'][3]*1e3
        ret['Description'] = data['Description']
        sec = data['3DX_Section'].strip()
        if len(sec) > 1 and sec[0:4]=='5002':
            sec ='ASM-'+sec
        ret['3DX-Section'] = sec
        par = data['3DX_Part']
        if len(par) > 1 and par[0:4]=='5002':
            par ='ASM-'+par
        ret['3DX-Part'] = par
        ret['3DX-Type'] = data['3DX_Type']
        ret['Parent'] = parent
        ret['Parent Offset'] =soff
        ret['X1 [mm]'] = data['P1'][1]*1e3
        ret['Y1 [mm]'] = data['P1'][2]*1e3-1200.
        ret['Z1 [mm]'] = data['P1'][3]*1e3
        ret['X2 [mm]'] = data['P2'][1]*1e3
        ret['Y2 [mm]'] = data['P2'][2]*1e3-1200.
        ret['Z2 [mm]'] = data['P2'][3]*1e3
        ret['Yaw'] = data['Yaw']
        ret['Pitch'] = data['Pitch']
        return ret