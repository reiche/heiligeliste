import pandas as pd
import xlsxwriter
#import xlwt
import numpy as np
import re

def writeReport(file1,file2,mismatch,unique,outfile):
    w = xlsxwriter.Workbook(outfile+'.xlsx')
    style0 = w.add_format({'bold': True, 'font_name': 'Arial'})

    # name of files
    ws = w.add_worksheet('Files')
    ws.set_column('A:A', 15)
    ws.set_column('B:B', 50)
    style = w.add_format({'font_name': 'Arial'})
    ws.write(0,0,'File1',style)
    ws.write(0,1,file1,style)
    ws.write(1,0,'File2',style)
    ws.write(1,1,file2,style)

    # tab with mismatch positions
    ws = w.add_worksheet('Position Mismatch')
    headers = ['NAME', 'x [mm]', 'y [mm]', 'z [mm]', 'Yaw [degree]','Pitch [degree]']
    ws.autofilter('A1:F1')
    ws.set_column('A:A', 22)
    ws.set_column('B:B', 10)
    ws.set_column('C:C', 10)
    ws.set_column('D:D', 10)
    ws.set_column('E:E', 10)
    ws.set_column('F:F', 10)

    for i, header in enumerate(headers):
        ws.write(0, i, header, style0)

    style = w.add_format({'font_name': 'Arial'})
    for row, entry in enumerate(mismatch):
        ws.write(row + 1, 0, entry['Name'],style)
        ws.write(row + 1, 1, '%9.3f' % entry['X'], style)
        ws.write(row + 1, 2, '%9.3f' % entry['Y'], style)
        ws.write(row + 1, 3, '%9.3f' % entry['Z'], style)
        ws.write(row + 1, 4, '%9.3f' % entry['Yaw'], style)
        ws.write(row + 1, 5, '%9.3f' % entry['Pitch'], style)

    # tab with unique elements
    ws = w.add_worksheet('Unique Elements')
    headers = ['Name']
    ws.autofilter('A1:A2')
    ws.set_column('A:A', 22)
    ws.set_column('B:B', 22)
    for i, header in enumerate(headers):
        ws.write(0, i, header, style0)
    style = w.add_format({'font_name': 'Arial'})
    for row, entry in enumerate(unique):
        ws.write(row + 1, 0, entry['Name'], style)
        ws.write(row + 1, 1, entry['File'], style)
    w.close()


class Export:
    def __init__(self):
        self.w = None
        self.orange = '#FFAA00'
        self.yellow = '#FFFF80'
        self.cyan = '#80FFFF'
        self.green = '#80FF80'
        self.purple = '#F080F0'
        self.lila = '#C0C0C0'
        self.grey = '#C5C5C5'
        self.sections={'SIN': 'Injector', 'S10': 'Linac 1', 'S20': 'Linac 2', 'S30': 'Linac 3',
                       'SAR': 'Aramis', 'SAT': 'Athos', 'SPO': 'Porthos'}

    def export(self,filename,PL,DL=None, Filter = None):
        self.open(filename)
        if DL:
            self.write3DXList(DL,Filter)
            if Filter:
                self.close()
                return
            self.writeDeviceList(DL)
        self.writeProtoList(PL)
        self.writeDipoleList(PL.dipole)
        self.close()

    def open(self, filename):
        self.w = xlsxwriter.Workbook(filename + '.xlsx')
        self.style0 = self.w.add_format({'bold':True,'font_name':'Arial'})
    def close(self):
        self.w.close()

    def writeDipoleList(self,dipoles):
        ws = self.w.add_worksheet('Dipoles')
        headers = ['Name', 'Angle', 'Alignment', 'Radius', 'Trans. Wander', 'Shift', 'Path Length']
        ws.autofilter('A1:G1')
        ws.set_column('A:A', 22)
        ws.set_column('B:B', 12)
        ws.set_column('C:C', 12)
        ws.set_column('D:D', 12)
        ws.set_column('E:E', 12)
        ws.set_column('F:F', 12)
        ws.set_column('G:G', 12)
        i = 0
        for header in headers:
            ws.write(0, i, header, self.style0)
            i = i + 1
        row = 1
        style = self.w.add_format({'bg_color': self.orange,'font_name': 'Arial'})
        for name in dipoles.keys():
            ws.write(row, 0, name.replace('.','-'), style)
            for i,field in enumerate(dipoles[name].keys()):
                if isinstance(dipoles[name][field],str):
                    ws.write(row, i+1, dipoles[name][field],style)
                else:
                    ws.write(row, i+1, '%8.4f' % dipoles[name][field],style)

            row = row + 1
    def write3DXList(self,layout, filter = '.*'):

        if not filter:
            filter = '.*'
        reg = re.compile(filter)
        ws=self.w.add_worksheet('3DX-HL')
        headers = ['NAME','TYPE','SECTION','3D_PART','S','XV','YV','ZV','ANGLE-H','ANGLE-V','DESCRIPTION']
        ws.autofilter('A1:P1')
        ws.set_column('A:A', 22)
        ws.set_column('B:B', 5)
        ws.set_column('C:C', 20)
        ws.set_column('D:D', 20)
        ws.set_column('E:E', 12)
        ws.set_column('F:F', 12)
        ws.set_column('G:G', 12)
        ws.set_column('H:H', 12)
        ws.set_column('I:I', 12)
        ws.set_column('J:J', 12)
        ws.set_column('K:K', 40)

        i = 0
        for header in headers:
            ws.write(0, i, header,self.style0)
            i = i + 1

        row = 1
        style = self.w.add_format({'font_name': 'Arial'})
        stylenum = self.w.add_format({'font_name': 'Arial', 'num_format': '0.000'})

        for entry in layout:
            if not reg.match(entry['Name']):
                continue
            if len(entry['3DX-Part']) > 2 or len(entry['3DX-Section']) > 2:
                sec=entry['3DX-Section']
                if len(sec) < 5:
                    if np.isnan(entry['S [m]']):
                        print('Invalid entry for element:', entry['Name'])
                        continue
                    ws.write(row, 0, entry['Name'], style)
                    part = entry['3DX-Part']
                    if 'ASM' not in part and 'PRD' not in part:
                        part = 'ASM-' + part
                    ws.write(row, 3, part, style)
                    ws.write(row, 4, entry['S [m]']*1000., stylenum)
                    ws.write(row, 5, entry['Zv [mm]'], stylenum)
                    ws.write(row, 6, entry['Xv [mm]'], stylenum)
                    ws.write(row, 7, entry['Yv [mm]'], stylenum)
                    ws.write(row, 8, entry['Yaw'], stylenum)
                    ws.write(row, 9, -entry['Pitch'], stylenum)
                else:
                    ws.write(row, 0, entry['Name'], style)
                    ws.write(row, 3, sec, style)
                ws.write(row, 10, entry['Description'], style)
                row = row + 1


    def writeDeviceList(self, layout):
        ws=self.w.add_worksheet('Device-HL')
        ws.autofilter('A1:P1')
        ws.set_column('A:A',22)
        ws.set_column('B:B',30)
        ws.set_column('C:C',8)
        ws.set_column('D:D',8)
        ws.set_column('E:E',8)
        ws.set_column('F:F',8)
        ws.set_column('G:G',8)
        ws.set_column('H:H',40)
        ws.set_column('I:I',20)
        ws.set_column('J:J',20)
        ws.set_column('K:K',8)
        ws.set_column('L:L',20)
        ws.set_column('M:M',8)
        ws.set_column('N:N',8)
        ws.set_column('O:O', 8)
        ws.set_column('P:P', 8)
        ws.set_column('Q:Q', 8)

        i = 0
        for header in layout[0].keys():
            ws.write(0, i, header,self.style0)
            i = i + 1
        row = 1
        style = self.w.add_format({'font_name': 'Arial'})
        for entry in layout:
            for i, key in enumerate(entry.keys()):
                if i == 0 and len(entry[key]) < 8:
                    ws.write(row, i, entry[key]+'-ZERO',style)
                else:
                    ws.write(row, i, entry[key],style)
            row = row + 1



    def writeProtoList(self, layout):
        ws=self.w.add_worksheet('Proto-HL')
        ws.autofilter('A1:N1')
        ws.set_column('A:A',14)
        ws.set_column('B:B',12)
        ws.set_column('C:C',6)
        ws.set_column('D:D',6)
        ws.set_column('E:E',9)
        ws.set_column('F:F',9)
        ws.set_column('G:G',9)
        ws.set_column('H:H',9)
        ws.set_column('I:I',9)
        ws.set_column('J:J',12)
        ws.set_column('K:K',12)
        ws.set_column('L:L',18)
        ws.set_column('M:M', 6)
        ws.set_column('N:N', 6)
        ws.set_column('O:O', 6)

        i = 0
        for header in layout.info['SINEG01'].keys():
            if i < 15:
                ws.write(0, i, header,self.style0)
            i = i + 1

        row = 1
        for ele in layout.order:
            color=self.orange
            group = layout.info[ele]['Group']
            if group == 'Section':
                color = self.grey
            if group == 'Diagnostics':
                color = self.yellow
            if group == 'RF':
                color = self.cyan
            if group == 'Vacuum' or group == 'Safety':
                color = self.green
            if group == 'ID':
                color = self.purple
            if group == 'Photonics':
                color = self.lila

            for i,key in enumerate(layout.info[ele].keys()):
                pref = layout.info[ele]['PV']
                if 'Start' in layout.info[ele]['Reference']:
                    pref = layout.info[ele]['P1']
                val = layout.info[ele][key]
                if i == 4:
                    val = pref[0]
                elif i == 5:
                    val = pref[3]
                elif i == 6:
                    val = pref[1]
                elif i == 7:
                    val = pref[2]
                format=''
                if i == 3:
                    format = '000'
                elif i > 3 and i < 8:
                    format='0.0000'
                if i < 15:
                    style = self.w.add_format({'bg_color': color, 'font_name': 'Arial', 'num_format': format})
                    ws.write(row,i,val,style)
            row = row + 1

    def revision(self):
        # import the revision
        excel_data = pd.read_excel('../Revision/Revision.xlsx')
        data = pd.DataFrame(excel_data)
        ws = w.add_worksheet('Revision')
        ws.set_column('A:A', 12)
        ws.set_column('B:B', 60)
        stylerev = w.add_format({'font_name': 'Arial'})
        for i in range(len(data.index)):
            if isinstance(data.iloc[i][0],str):
                ws.write(i,0,data.iloc[i][0],stylerev)
            if isinstance(data.iloc[i][1], str):
                ws.write(i, 1, data.iloc[i][1], stylerev)

