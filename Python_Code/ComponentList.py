import pandas
import os
import numpy as np

class ComponentList:

    def __init__(self):
        self.data = None
        self.variant = None
        self.section = None
        self.dipoles = None
        self.hasDipoles = False

        self.cad={}
        self.comp={}  # components defined by TYPE (Baugruppe)
        self.ccomp={} # components define by CNAME


    def readFiles(self,path,phase):
        filecomp = os.path.join(path,'HL-Components','HL-Components.xlsx')
        print('Parsing:', filecomp)
        self.parseNewComponents(filecomp)
#
        filesect = os.path.join(path,'HL-Components','SectionTable.xlsx')
        print('Parsing:',filesect)
        phases=['Current','Planned','Final']
        tabname='Phase %s' % phases[phase]
        self.section = pandas.read_excel(filesect, sheet_name=tabname)

        filesect = os.path.join(path, 'HL-Components', 'DipolesTable.xlsx')
        if os.path.exists(filesect):
            print('Parsing:', filesect)
            self.dipoles = pandas.read_excel(filesect, sheet_name='Dipoles')
            self.hasDipoles = True
        else:
            print('Dipole Excel File not found - Proceeding with Proto List Info')
            self.dipoles = None
            self.hasDipoles = False

    def parseNewComponents(self,filecomp):
        data = pandas.read_excel(filecomp, sheet_name='New-Components')
        self.comp.clear()
        filter=data['TYPE'].isnull()
        data['NAMES'].fillna('', inplace=True)
        data['3DX_PART'].fillna('', inplace=True)
        for i,val in enumerate(filter):
            if not val:
                key=data.iloc[i,1]
                if key in self.comp.keys():
                    print('Duplicate definition of component type:',key)
                self.comp[key]={'CAT':data.iloc[i,0],'CNAME':data.iloc[i,2],'3DX_PART':data.iloc[i,3],
                                'DESC':data.iloc[i,4],'NAMES':data.iloc[i,5].split(',')}


    def find3DXType(self,part):
        # this routine is obsolete!
        if part in self.cad.keys():
            return self.cad[part]
        else:
            return ''

    def findProtoElement(self,name,baugruppe):
        if 'CELL' in name:
            return None
        res = []
        for key in self.comp.keys():
            if key == baugruppe:
                return self.comp[key]
            if (baugruppe+'.') in key:
                res.append(key)
        if len(res) == 0:
            print('*** Error: Cannot find element',name, 'with baugruppe', baugruppe, 'in component list' )
            return None
        for ele in res:
            names = self.comp[ele]['NAMES']
            found = [True for i in names if name in i]
            if True in found:
                return self.comp[ele]
        # defaulting to first baugruppe
        nbg = baugruppe+'.1'
        res = self.findProtoElement(name,nbg)
        if res is None:
            print('*** Error: Cannot find element', name, 'with baugruppe', baugruppe, 'in component list')
        else:
            print('*** Info: Defaulting element',name,'to baugruppe',nbg)
        return res

    def find_section(self,name='SINEG01'):
        sec = self.section[self.section['Prefix'] == name]
        if sec.empty:
            print('Cannot find 3DX Section:',name)
            return None
        return sec

    def find_dipole(self,name='SINLH01-MBND100'):
        if not self.hasDipoles:
            return None
        dip = self.dipoles[self.dipoles['Name']== name]
        if dip.empty:
            print('Cannot find in DipoleTable.xlsx the entry for:', name)
            print('    (Defaulting to orientation of Proto-List)')
            return None
        return dip