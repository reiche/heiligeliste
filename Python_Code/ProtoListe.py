import sys

from onlinemodel.core.apptemplate import ApplicationTemplate
from onlinemodel.core.facility import Facility
import math
import numpy as np

class ProtoListe(ApplicationTemplate):
# any calculation for the explicit layout of the machine    

    def __init__(self,phase = 0):
        self.SF = Facility(1,phase)   # generate instance of facility
        self.phase = phase
        choice = ['Current', 'Planned', 'Future']
        self.phaseName=choice[phase]
        self.info={}
        self.order=[]
        self.dipole={}



    def generateLayout(self):
        print('--------\nGenerating Layout for Phase: %s\n' % self.phaseName)
        self.order.clear()
        self.info.clear()
        self.dipole.clear()
        self.setRef(-0.085, -0.085)
        self.SF.writeFacility(self)

    def setRef(self,sin=0,zin=0,xin=0,yin=1.2,exin=0,eyin=0,ezin=1):
        self.s=sin
        self.z=zin
        self.x=xin
        self.y=yin
        # vector of direction
        self.ex=exin
        self.ey=eyin
        self.ez=ezin
        
        self.vertx=0
        self.verty=0
        self.vertz=0
        self.verts=0

        # some stuff to make it easier to generate the holy list.
        self.p1 = np.array([sin,xin,yin,zin]) # starting point
        self.pv = np.array([0,0,0,0])  # center point/vertex point
        self.p2 = np.array([0,0,0,0])  # end point
        self.ev1 = np.array([1,exin,eyin,ezin])
        self.ev2 = np.array([1,exin,eyin,ezin])
        self.ev = np.array([0,0,0])


    def advance(self, L, angle=0, tilt=0): # this is independent on the new coordinate system for the holy list
        self.vertx = 0
        self.verty = 0
        self.vertz = 0
        self.verts = self.s
        self.s = self.s + L
        if angle == 0:
            self.x = self.x + L * self.ex
            self.y = self.y + L * self.ey
            self.z = self.z + L * self.ez
        else:
            # save initial coordinates
            xtmp = self.x
            ytmp = self.y
            ztmp = self.z
            # radius
            R = L / angle
            # rotation vector - default around y axis
            Rproj = self.ey  # R dot e = Ry*ey=ey
            Rx = -Rproj * self.ex
            Ry = 1 - Rproj * self.ey
            Rz = -Rproj * self.ez
            # Orthonormal vector to direction of propagation and rotation axis
            Ox = Ry * self.ez - Rz * self.ey
            Oy = Rz * self.ex - Rx * self.ez
            Oz = Rx * self.ey - Ry * self.ex
            Rnorm = math.sqrt(Rx * Rx + Ry * Ry + Rz * Rz)
            Onorm = math.sqrt(Ox * Ox + Oy * Oy + Oz * Oz)
            Rx = Rx / Rnorm
            Ry = Ry / Rnorm
            Rz = Rz / Rnorm
            Ox = Ox / Onorm
            Oy = Oy / Onorm
            Oz = Oz / Onorm
            # Rot Vector is the axis of rotation and Ort vector pointing to the origin of the rotation
            Ortx = Ox * math.cos(tilt) - Rx * math.sin(tilt)
            Orty = Oy * math.cos(tilt) - Ry * math.sin(tilt)
            Ortz = Oz * math.cos(tilt) - Rz * math.sin(tilt)
            # the origin of the rotation
            X0 = self.x + R * Ortx
            Y0 = self.y + R * Orty
            Z0 = self.z + R * Ortz
            # rotation

            self.vertx = X0 - R * (Ortx * math.cos(angle * 0.5) - self.ex * math.sin(angle * 0.5)) / math.cos(angle * 0.5)
            self.verty = Y0 - R * (Orty * math.cos(angle * 0.5) - self.ey * math.sin(angle * 0.5)) / math.cos(angle * 0.5)
            self.vertz = Z0 - R * (Ortz * math.cos(angle * 0.5) - self.ez * math.sin(angle * 0.5)) / math.cos(angle * 0.5)
            self.x = X0 - R * (Ortx * math.cos(angle) - self.ex * math.sin(angle))
            self.y = Y0 - R * (Orty * math.cos(angle) - self.ey * math.sin(angle))
            self.z = Z0 - R * (Ortz * math.cos(angle) - self.ez * math.sin(angle))
            self.ex = (self.ex * math.cos(angle) + Ortx * math.sin(angle))
            self.ey = (self.ey * math.cos(angle) + Orty * math.sin(angle))
            self.ez = (self.ez * math.cos(angle) + Ortz * math.sin(angle))
            # calculate vertex
            ds = (self.vertx - xtmp) * (self.vertx - xtmp) + (self.verty - ytmp) * (self.verty - ytmp) + (
                        self.vertz - ztmp) * (self.vertz - ztmp)
            self.verts = self.verts + math.sqrt(ds)


    def swap(self):
            xtmp=self.x
            ytmp=self.y
            ztmp=self.z
            stmp=self.s
            self.x=self.vertx
            self.y=self.verty
            self.z=self.vertz
            self.s=self.verts
            self.vertx=xtmp
            self.verty=ytmp
            self.vertz=ztmp
            self.verts=stmp


    def getDomain(self,name):
        domain = 'INJECTOR'
        if name[1:3] == '10':
            domain = 'LINAC1'
        if name[1:3] == '20':
            domain = 'LINAC2'
        if name[1:3] == '30':
            domain = 'LINAC3'
        if name[1:3] == 'AR':
            domain = 'ARAMIS-U15'
        if name[1:3] == 'AT':
            domain = 'ATHOS-U40'
        if name[1:3] == 'PO':
            domain = 'PORTHOS'
        return domain

    def write(self,elements):


        name=elements[0]
        reference=elements[2]
        elelength=elements[1]
        group=elements[3]
        prefix = name[0:7]
        suffix = name[8:12]
        index = int(name[12:15])
        baugruppe = elements[5]
        roll = elements[6]
        psch = elements[7]

        align = 'Entrance'
        namePL=name.replace('-','.')
        if namePL in self.dipole.keys():
            align = self.dipole[namePL]['Alignment']

        domain = self.getDomain(name)

        if (name=='SINEG01.MSOL010'):    ############### very first element - very ugly implementation
             self.setRef(-0.085,-0.085)
             self.pv=self.p1
             self.p1 = self.pv - 0.5 * elelength*self.ev1
             self.p2 = self.pv + 0.5 * elelength*self.ev1


        angh=np.arctan2(self.ev[0],self.ev[2])*180/np.pi
        rh = np.sqrt(self.ev[2]*self.ev[2]+self.ev[0]*self.ev[0])
        angv=np.arctan2(self.ev[1],rh)*180/np.pi

        if not prefix in self.info.keys():
            info = {'Domain': domain, 'Prefix': prefix, 'Suffix': '', 'Index': '',
                    's (m)': 0, 'z (m)': 0, 'x (m)': 0, 'y (m)': 0, 'L (m)': 0,
                    'Reference': 'Start', 'Group': 'Section', 'Baugruppe': 'Marker', 'Variante':'A', 'Roll': 0,
                    'PS-Ch.': 0, 'P1': self.p1, 'PV': self.p1, 'P2': self.p1, 'EV1': self.ev1,
                    'EV2': self.ev1,'Yaw':angh,'Pitch':angv, 'Alignment': align}
            self.info[prefix] = info
            self.order.append(prefix)


        pos_s=self.s
        pos_z=self.z

        if name in self.order:
            return

        if name[8:9]=='S':
            group='Safety'

        # add some ad hoc shifts of elements
        ds = np.array([0,0,0,0])


        if name[0:12]=='SINBC02.DCOL' or name[0:12]=='S10BC02.DCOL' or name[0:12]=='S10BC02.DSFH':
            reference='Center'
            pos_s=pos_s+0.5*elelength
            pos_z=pos_z+0.5*elelength
            ds = +0.5*elelength * np.array([1, 0, 0, 1])

        if name[0:12]=='SINBC02.DSCR' or name[0:12]=='S10BC02.DSCR':
            elelength=0.28
            pos_s=pos_s-0.09
            pos_z=pos_z-0.09
            ds = -0.09 * np.array([1,0,0,1])



        info={'Domain': domain, 'Prefix': prefix, 'Suffix': suffix, 'Index': index,
              's (m)': pos_s, 'z (m)': pos_z, 'x (m)': self.x, 'y (m)': self.y, 'L (m)': elelength,
              'Reference': reference, 'Group': group, 'Baugruppe': baugruppe, 'Variante': 'A', 'Roll': roll,
              'PS-Ch.': psch,'P1':self.p1+ds,'PV':self.pv+ds,'P2':self.p2+ds,'EV1':self.ev1,'EV2':self.ev2,
              'Yaw': angh, 'Pitch': angv, 'Alignment': align}

        self.info[name] = info
        self.order.append(name)

#----------------------------------------
# specific elements

    def isType(self,name):
        if (name.find('holylist')>-1):
            return 1
        else:
            return 0

    def writeLine(self,ele,seq):
        if not ele.Name in self.info.keys() and len(ele.Name) > 1:
            print('Empty Cell encountered:', ele.Name)
            domain = self.getDomain(ele.Name)
            align = 'Entrance'
            angh = np.arctan2(self.ev[0], self.ev[2]) * 180 / np.pi
            rh = np.sqrt(self.ev[2] * self.ev[2] + self.ev[0] * self.ev[0])
            angv = np.arctan2(self.ev[1], rh) * 180 / np.pi
            info = {'Domain': domain, 'Prefix': ele.Name, 'Suffix': '', 'Index': '',
                    's (m)': self.s, 'z (m)': self.z, 'x (m)': self.x, 'y (m)': self.y, 'L (m)': 0,
                    'Reference': 'Start', 'Group': 'Section', 'Baugruppe': 'Marker', 'Variante': 'A', 'Roll': 0,
                    'PS-Ch.': 0, 'P1': self.p1, 'PV': self.p1, 'P2': self.p1, 'EV1': self.ev1,
                    'EV2': self.ev1, 'Yaw': angh, 'Pitch': angv, 'Alignment': align}
            self.info[ele.Name] = info
            self.order.append(ele.Name)
        return

    def writeDrift(self,s):
        self.p1 = self.p1 + s * self.ev1   # no change in vector  - should replace advance in the future
        self.advance(s,0)

    def writeAlignment(self,ele):
        # print info of the element and advances the layout position over the reserved length
        if 'dx' in ele.__dict__:
            self.x=self.x+ele.dx
            self.p1[1] += ele.dx
        if 'dy' in ele.__dict__:
            self.y=self.y+ele.dy
            self.p1[2] += ele.dy
        self.advance(ele.getResLength(),0)


    def writeMarker(self,ele):
        return

    def writeVacuum(self,ele): #  all components, which have a well defined vacuum vessel.
        self.p2 = self.p1 + ele.LengthRes*self.ev1
        self.pv = self.p1
        self.ev2 = self.ev1
        self.ev = self.ev1[1:4]
        self.write((ele.Name,ele.LengthRes,'Start',ele.Group,ele.Tag,ele.Baugruppe,0,0))
        self.p1 = self.p2 # advance position
        self.advance(ele.LengthRes,0)

    def writeRF(self,ele):
        self.writeVacuum(ele)

    def writeDiagnostic(self,ele):
        self.writeVacuum(ele)

    def writeUndulator(self,ele):
        self.writeMagnets(ele)

    def writeCorrector(self,ele):
        self.writeMagnets(ele)

    def writeQuadrupole(self,ele):
        self.writeMagnets(ele)

    def writeSextupole(self,ele):
        self.writeMagnets(ele)

    def writeSolenoid(self,ele):
        self.writeMagnets(ele)



    def writeMagnets(self,ele):
        channels=0
        if 'k1' in ele.__dict__:
            channels=channels+1    
        if 'k2' in ele.__dict__:
            channels=channels+1 
        if 'ks' in ele.__dict__:
            channels=channels+1
        if 'corx' in ele.__dict__:
            channels=channels+1    
        if 'cory' in ele.__dict__:
            channels=channels+1 
        if 'cor' in ele.__dict__:
            channels=channels+1    
        if ele.Baugruppe=='QFU':
            channels=channels-1
        if ele.Baugruppe=='QFUE':
            channels=channels-1
        if ele.Baugruppe=='UE38-DELAY':
            channels=channels+1
        # print info of the element and advances the layout position over the reserved length
        self.advance(ele.sRef+ele.Length*0.5,0) # propagation to the center point
        self.p1 = self.p1 + ele.sRef*self.ev1
        self.pv = self.p1 + 0.5 * ele.Length*self.ev1
        self.p2 = self.p1 + ele.Length*self.ev1
        self.ev2 = self.ev1
        self.ev = self.ev1[1:4]
        self.write((ele.Name,ele.Length,'Center',ele.Group,ele.Tag,ele.Baugruppe,ele.Tilt*90/math.asin(1),channels))
        self.p1 = self.p2 + (ele.LengthRes-ele.sRef-ele.Length)*self.ev1
        self.advance(ele.LengthRes-ele.sRef-ele.Length*0.5,0) # residual space including half the magnet length
        




    def writeBend(self,ele):
        # print info of the element and advances the layout position over the reserved length
        channels=1
        if ele.Baugruppe[0:3]=='AFP':
            channels=0
        if 'cor' in ele.__dict__:
            channels=channels+1    
        self.advance(ele.sRef,0)
        self.p1 = self.p1 + ele.sRef*self.ev1
        if (ele.design_angle==0):
            self.pv = self.p1 + 0.5*ele.getLength()*self.ev1
            self.p2 = self.p1 + ele.getLength()*self.ev1
            self.ev2 = self.ev1
            self.ev= self.ev1[1:4]
            self.advance(0.5*ele.getLength(),0)
            if ele.Name not in self.dipole.keys():
                self.dipole[ele.Name] = {'Angle': ele.design_angle, 'Alignment': 'Entrance', 'Radius': '',
                                         'Transverse Wander': 0, 'Shift': 0, 'Length': ele.getLength()}
            self.write((ele.Name, ele.getLength(), 'Vertex', ele.Group, ele.Tag, ele.Baugruppe,
                        ele.Tilt * 90 / math.asin(1), channels))
            self.p1 = self.p2+(ele.getResLength()-ele.sRef-ele.getLength())*self.ev1
            self.advance(ele.getResLength()-ele.sRef-0.5*ele.getLength(), 0)
        else:
 #           print('Bending magnet', ele.Name,ele.angle,ele.design_angle,ele.sRef)
            xtmp=self.x
            ytmp=self.y
            ztmp=self.z
            stmp=self.s
            extmp=self.ex
            eytmp=self.ey
            eztmp=self.ez
            angletmp=ele.angle
            ele.angle=ele.design_angle
            angrad = np.pi* ele.angle / 180.

            # ignoring tilt:
            R = ele.getLength()/angrad
            M = np.zeros((4,4))

            M[0,0]=1
            M[2,2]= 1
            M[1, 1] = np.cos(angrad)
            M[3, 3] = np.cos(angrad)
            M[1, 3] = np.sin(angrad)
            M[3, 1] = -np.sin(angrad)
            self.ev2 = np.matmul(M,self.ev1)
            if not ele.Tilt == 0:
                vec1 = np.array(self.ev1[1:])
                vec2 = np.array(self.ev2[1:])
                proj = np.sum(vec1*vec2)
                vecper = vec2 - proj*vec1
                vecpar = vec2 - vecper
                veccro = np.cross(vec1,vecper)
                self.ev2[1:] = vecpar+np.cos(ele.Tilt)*vecpar-np.sin(ele.Tilt)*veccro

            Ld = R * np.sin(angrad*0.5)
            T = Ld/np.cos(angrad*0.5)
            self.pv = self.p1 + T * self.ev1
            self.p2 = self.pv + T * self.ev2
            self.ev = (1-ele.e1) * self.ev1[1:4] + (1-ele.e2) * self.ev2[1:4]

            # calculate shifts of dipoles for good field region
            R = ele.getLength() / angrad;
            dR = R * (1 - math.cos(angrad))  # beam wander
            shift = 0.5
            if (ele.e1 == 0.5):
                dR = R * (1 - math.cos(angrad * 0.5))
                shift = -0.5
            vec1 = np.array(self.ev1[1:])
            vec2 = np.array(self.ev2[1:])
            vnorm = np.cross(vec1, vec2)
            vrot = (1 - ele.e1) * vec1 + (1 - ele.e2) * vec2
            vshift = np.cross(vnorm, vrot)
            vlen = np.sqrt(np.sum(vshift*vshift))
            # ###############################
            # disable shift in holy list!!!!
            #
            vlen = 0

            if vlen > 0:
                vshift = vshift /vlen
                eshift = np.sign(angrad)*np.array([0,vshift[0],vshift[1],vshift[2]])*shift*dR
                self.p1 += eshift
                self.pv += eshift
                self.p2 += eshift

            # advance according to design angle
            self.advance(ele.getLength(), angrad, ele.Tilt)
            self.swap()

            if ele.Name not in self.dipole.keys():
                if ele.e2 == 1:
                    align = 'Entrance'
                elif ele.e1 ==1:
                    align = 'Exit'
                else:
                    align = 'Middle'
                self.dipole[ele.Name] = {'Angle': ele.design_angle, 'Alignment': align, 'Radius': np.abs(R),
                                         'Transverse Wander': np.abs(dR), 'Shift': shift*dR, 'Length': ele.getLength()}

            self.write((ele.Name, ele.Length, 'Vertex', ele.Group, ele.Tag, ele.Baugruppe, ele.Tilt * 90 / math.asin(1), channels))
            self.swap()

            if vlen > 0:
                self.p1 -= eshift
                self.pv -= eshift
                self.p2 -= eshift

            # restore position before dipole and then advance according to given angle (this matters for branching point dipoles
            self.x=xtmp
            self.y=ytmp
            self.z=ztmp
            self.s=stmp
            self.ex=extmp
            self.ey=eytmp
            self.ez=eztmp





            ele.angle=angletmp
            if ele.angle == 0:
                self.p1 = self.p1 + ele.getLength() *self.ev1
            else:
                ps = self.p1[0]+ele.getLength()
                self.p1 = self.p2
                self.p1[0]=ps
                self.ev1 = self.ev2
            angrad=np.pi*ele.angle/180.
            Lpath=ele.getLength()
            self.advance(Lpath,angrad,ele.Tilt)
            self.advance(ele.getResLength()-ele.sRef-Lpath,0)  
            self.p1  = self.p1 + (ele.getResLength()-ele.sRef-Lpath)*self.ev1




