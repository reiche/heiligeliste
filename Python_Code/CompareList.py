import pandas as pd
import numpy as np

from WriteExcel import writeReport

def compareLists(args):
    """
    Compares two files for differences and consistency
    :param args: command line arguments
    :return: None
    """

    outfile='report'
    if len(args)>2:
        outfile=args[-1]
    data1,dtype1 = checkFile(args[0])
    data2,dtype2 = checkFile(args[1])
    if dtype1 or dtype2:   # check if one file is 3DX layout -> use only first tab
        lat1 = parseData(data1,dtype1,0)
        lat2 = parseData(data2,dtype2,0)
        print('')
        print('****************************')
        print('Comparing 3DXperience Layout')
        uniq,pos=compareWith3DXExport(lat1,lat2)
        writeReport(args[0],args[1],pos,uniq,outfile)
        return
    # compare two Holy list with each other. Since 3DX tab is a subset of the HolyList only tab 1 and 2 ar eused

    # proto list
    lat1 = parseData(data1, dtype1, 2)
    lat2 = parseData(data2, dtype2, 2)
    print('')
    print('****************************')
    print('Comparing Proto Holy List Layout')
    uniq, pos = compareProto(lat1, lat2)
#    writeReport(args[0], args[1], pos, uniq, outfile)

    # holy list
#    lat1 = parseData(data1, dtype1, 1)
#    lat2 = parseData(data2, dtype2, 1)

def parseData(data,ftype,sheet):
    """
    Convert a given panda data sheet into a dictionary
    :param data: panda data_frame. In case of holylist it is a list of data frames
    :param ftype: bool if data_fram is form 3DX export or not
    :param sheet: used data sheet of the data frame
    :return: dictionary of lattice elements
    """
    lat = {}
    if ftype:
        for index, row in data.iterrows():
            if len(row['NAME']) > 1:
                lat[row['NAME']] = {'section': row['3D_SECTION'], 'part': row['3D_PART'],
                                     'x': row['XV'], 'y': row['YV'], 'z': row['ZV'],
                                     'yaw': row['ANGLE_H'], 'pitch': row['ANGLE_V']}
    elif sheet==0:
        for index, row in data[0].iterrows():
            lat[row['NAME']] = {'section': row['SECTION'], 'part': row['3D_PART'],
                                 'x': row['XV'], 'y': row['YV'], 'z': row['ZV'],
                                 'yaw': row['ANGLE-H'], 'pitch': row['ANGLE-V']}
    elif sheet ==1:
        a=1
    elif sheet==2:
        for index, row in data[2].iterrows():
            name ='%s-%s%s' % (row['Prefix'], row['Suffix'], row['Index'])
            lat[name] = {'x': row['x (m)'], 'y': row['y (m)'], 'z': row['z (m)'], 's': row['s (m)']}

    return lat
def checkFile(file):
    """
    Reads excel file and check for 3DX export
    :param file: filename of Excel file
    :return: dataset or None
    """
    data = pd.read_excel(file, sheet_name=0)
    if 'Status' in data.columns:
        print('File: ',file,'is 3DXperience Export File')
        data['NAME'].fillna('', inplace=True)
        return data, True
    else:
        print('File: ',file,'is Holy List File')
        data1 = pd.read_excel(file, sheet_name=1)
        data2 = pd.read_excel(file, sheet_name=2)
        data2['Index'].fillna('', inplace=True)
        data2['Suffix'].fillna('', inplace=True)
        return [data,data1,data2], False

def compareProto(lat1,lat2):
    """
    Compares Proto List tab  for two holy Lists
    :param lat1: proto lattice 1
    :param lat2: proto lattice 2
    :return: lists of missing element and position difference
    """

    # step 2 - report on non matching elements and remove them from the list
    print('Checking for Missing Elements')
    unique1 = []
    for key in lat1.keys():
        if not key in lat2.keys():
            if not key:
                unique1.append({'Name': key, 'File': 'File1'})  # note that for this only the first file is checked
                print('  ', key, 'in first file')
    for key in unique1:
        del lat1[key['Name']]

    unique2 = []
    for key in lat2.keys():
        if not key in lat1.keys():
            if not key:
                unique2.append({'Name': key, 'File': 'File2'})  # note that for this only the first file is checked
                print('  ', key, 'in second file')
    for key in unique2:
        del lat2[key['Name']]
    unique = []

    # step 3 - check for position mismatch
    posmis = []
    print('Checking for Position Mismatch')
    for key in lat1.keys():
        dx = lat1[key]['x'] - lat2[key]['x']
        dy = lat1[key]['y'] - lat2[key]['y']
        dz = lat1[key]['z'] - lat2[key]['z']
        dp = lat1[key]['s'] - lat2[key]['s']
        ds = np.sqrt(dx * dx + dy * dy + dz * dz + dp * dp)
        if ds > 50e-6:  # threshold is 50 micron difference or 0.01 degree rotation
            posmis.append({'Name': key, 'X': dx, 'Y': dy, 'Z': dz, 'S': dp})
            print('  ', key, 'dx:', dx, 'dy:', dy, 'dz:', dz, 'ds:', dp)

    return unique, posmis


def compareWith3DXExport(lat1,lat2):
    """
    Compares 3DX layout of either holy list or 3DXperience export
    The routine checks only for the elements in the first lattice since they can be significantly shorter than the full
    holy list. An excel file is generated as the report.
    :param lat1: shorter lattice to be checked against the second lattice
    :param lat2: reference lattice
    :return: lists of missing element and position difference
    """

    # step 2 - report on non matching elements and remove them from the list
    print('Checking for Missing Elements')
    unique = []
    for key in lat1.keys():
        if not key in lat2.keys():
            if not key:
                unique.append({'Name':key,'File':'File1'})   # note that for this only the first file is checked
                print('  ',key)
    for key in unique:
        del lat1[key]

    # step 3 - check for position mismatch
    posmis=[]
    print('Checking for Position Mismatch')
    for key in lat1.keys():
        dx = lat1[key]['x'] - lat2[key]['x']
        dy = lat1[key]['y'] - lat2[key]['y']
        dz = lat1[key]['z'] - lat2[key]['z']
        ds = np.sqrt(dx*dx+dy*dy+dz*dz)
        darg1 = lat1[key]['yaw'] - lat2[key]['yaw']
        darg2 = lat1[key]['pitch'] - lat2[key]['pitch']
        darg=np.sqrt(darg1*darg1+darg2*darg2)
        if ds > 50e-3 or darg > 0.01:  # threshold is 50 micron difference or 0.01 degree rotation
            posmis.append({'Name':key,'X':dx,'Y':dy,'Z':dz,'Yaw':darg1,'Pitch':darg2})
            print('  ',key,'dx:',dx,'dy:',dy,'dz:',dz,'yaw:',darg1,'pitch',darg2)

    return unique,posmis




def compareAmongLists(file1,file2):
    print('Hello World')


def compareWithOldHL(file1,file2):
    data1 = pd.read_excel(file1, sheet_name= 1)  # this is the generated device list
    data2 = pd.read_excel(file2, sheet_name = 0) # this is the former holy list by Hubert Lutz

    data1['Parent'].fillna('', inplace=True)

    print('Comparing Holy Lists...')


    # step 1 - extract relevant elements from the exeel table
    lat1={}
    lat2={}
    for index,row in data1.iterrows():
        lat1[row['Name']] = {'x': row['Xv [mm]'], 'y': row['Yv [mm]'], 'z': row['Zv [mm]'], 's': row['S [m]'], 'Parent':row['Parent']}
    for index, row in data2.iterrows():
        lat2[row['NAME']] = {'x': row['X']*1e3, 'y': row['Y']*1e3, 'z': row['Z']*1e3, 's': row['S'], 'Parent':''}



    # step 2 - report on non matching elements and remove them from the list
    print('Checking for Missing Elements')
    unique1=[]
    for key in lat1.keys():
        if not key in lat2.keys():
            unique1.append(key)
    for key in unique1:
        del lat1[key]


    unique2=[]
    for key in lat2.keys():
        if not key in lat1.keys():
            unique2.append(key)
    for key in unique2:
        del lat2[key]

    # step 3 - check for position mismatch
    posmis=[]
    print('Checking for Position Mismatch')
    for key in lat1.keys():
        dx = lat1[key]['x']-lat2[key]['x']
        dy = lat1[key]['y'] - lat2[key]['y']
        dz = lat1[key]['z'] - lat2[key]['z']
        ds = np.sqrt(dx*dx+dy*dy+dz*dz)
        dds = lat1[key]['s']-lat2[key]['s']
        if ds > 50e-3:  # threshold is 50 micron difference
            posmis.append({'Name':key,'X':dx,'Y':dy,'Z':dz,'S':dds,'Parent':lat1[key]['Parent']})

    print('Writing Report')
    writeReport(file1,file2,posmis,unique1,unique2)



#    reportUnique(unique1,file1)
#    reportUnique(unique2,file2)


def reportUnique(uni,file):
    if len(uni) > 0:
        print ('Unique Elements in %s' % file)
        for un in uni:
            print('  ',un)

def unused():
    # check first for missing elements from assembly list
    print('Missing elements from Holy List:')
    for index,row in data2.iterrows():
        match = data1.loc[data1['Name'] == row['NAME']]
        if len(match.index) == 0  and row['S'] < 3.:
            print('  ',row['NAME'])
    # check if existing entries are consistent
    print('Inconsistent elements')
    for index,row in data1.iterrows():
        match = data2.loc[data2['NAME'] == row['Name']]
        if len(match.index) > 0:
            x1 = row['Xv [mm]']
            y1 = row['Yv [mm]']
            z1 = row['Zv [mm]']
            x2 = match.iloc[0]['X']
            y2 = match.iloc[0]['Y']+ 1.2
            z2 = match.iloc[0]['Z']
            d2 = np.sqrt((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)
            s1 = row['S [m]']
            s2 = match.iloc[0]['S']
            ds = np.abs(s1-s2)
#            if d2 > 1e-6 or ds > 1e-6:
#                print(row['Name'],'- Position mismatch (mm):',np.sqrt(d2),'(Parent:', row['Parent'],')')
#                if np.abs(x1-x2)> 1e-6:
#                    print('X:', x1 - x2)
#                if np.abs(y1 - y2) > 1e-6:
#                    print('Y:', y1 - y2)
#                if np.abs(z1-z2)> 1e-6:
#                    print('Z:', z1 - z2)
#                if np.abs(s1-s2)> 1e-6:
#                    print('S:', s1 - s2)
            if not match.iloc[0]['CNAME'][3:] == row['Type'][3:]:
                print(row['Name'],'- Type Mismatch:\n  Old Holy List:',match.iloc[0]['CNAME'],'\n  Proto/Component/Assembly List:',row['Type'])
        else:
            print('Element in Proto List not found in Holy List;', row['Name'], '(Parent:', row['Parent'],')')
#Raw version of generating