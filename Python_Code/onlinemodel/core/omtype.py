import math 

#-------------------------------------
# Type Manager - exclusively called by OMLayout

class TypeManager:
    def __init__(self):
        self.library={}  #----- dictionary to contain all the element and line definition
        self.typecase={'Solenoid':self.sol,'Photonics':self.xray,'Undulator':self.ID,'Vacuum':self.vac, \
                       'Quadrupole':self.quad,'Corrector':self.cor,'Diagnostic':self.diag,'RF':self.rf, \
                       'Dipole':self.bend,'Sextupole':self.sext, 'Marker':self.marker}

    def info(self):
        for ele in self.library.keys():
            eledef=self.library[ele]
            for att in eledef.keys():
                if isinstance(eledef[att],str):
                    print('\t',att," = '",eledef[att],"'")
                else:
                    print('\t',att,' = ',eledef[att])
            print('\n')

   
    def define(self,name,properties):   #----------- Routine to call when an element or line is defined      
        indict={name:properties}
        self.library.update(indict)
        
    def generate(self,name,index=0,option={}):  # create an instance of the object based on the content of the dictionary 'library'
        prop=self.library[name]    
				
        #print ("name=", name)				
        #print ("prop=", prop)				
				
				#Convert tuple to list
        if isinstance(prop,tuple):
            prop=list(prop) 
            print ("tuple======== REPORT THIS PLEASE TO JAN CHRIN ======= ")
				
        if isinstance(prop,list):   #--------- is a line-----------------
            #print ("list")
            prop0=prop[0]
            L=0
            if 'Length' in prop0: #prop0.has_key('Length'):
                L=prop0['Length']
            name=''    
            if 'Name' in option: #option.has_key('Name'):
                name=option['Name'] 
            obj=LineContainer(name,L)
            for ele in prop[1:len(prop)]:
                eletype=ele['Element']
                sRef=ele['sRef']
                index=0
                if 'index' in ele: #ele.has_key('index'):
                    index=ele['index']
                if 'Option' in ele: #ele.has_key('Option'):
                    opt=ele['Option']
                else:
                    opt={}
                rel='absolute'
                if 'Ref' in ele: #ele.has_key('Ref'):
                    if ele['Ref']=='relative':
                        rel='relative' 
                obj.append(self.generate(eletype,index,opt),sRef,rel)
        else:    
            #print ("else other")                                    
            obj=self.typecase[prop['Type']](prop)  # is a simple element 
            option['index']=index
            obj.__dict__.update(option)
        return obj      
    
    def xray(self,prop):
        return Photonic(prop)   
    
    def ID(self,prop):
        return Undulator(prop)   

    def vac(self,prop):
        return Vacuum(prop)   

    def quad(self,prop):
        return Quadrupole(prop)   
    
    def bend(self,prop):
        return Dipole(prop)   

    def sol(self,prop):
        return Solenoid(prop)   

    def sext(self,prop):
        return Sextupole(prop)   

    def cor(self,prop):
        return Corrector(prop)
    
    def diag(self,prop):
        return Diagnostic(prop)  
    
    def rf(self,prop):
        return RF(prop)
    
    def marker(self,prop):
        return Marker(prop)          

                           

#-----------------------------------------------------------------------------
# Line Container 


class LineContainer:
    def __init__(self, namein='', Lin=0):
        self.Name=namein
        self.LengthRes=Lin
        self.Ref={}            
        self.Element=[]
        self.sRef=0    
        self.initiated=0
        self.output=1
        self.isFlat=0

    def getLength(self):
        return self.getResLength()

    def getResLength(self):       
        if len(self.Element)==0:
            return self.LengthRes
        ele=self.Element[-1]
        slen=self.position(ele)+ele.getLength()
        if (self.LengthRes<0):
            return slen-self.LengthRes
        else:
            if (self.LengthRes<slen):
                return slen
            else:
                return self.LengthRes
            
               
    def position(self,ele): # Beginning of the element (Reserve length inclusive)
        Ref=self.Ref[ele]
        if Ref['ref']=='absolute':
            return Ref['pos']
        else:
            idx=self.Element.index(ele)-1
            srel=0
            if idx>=0:
                srel=self.position(self.Element[idx])+self.Element[idx].getResLength()
            if Ref['ref']=='relative':
                return srel+Ref['pos']
            else:
                idx=Ref['pos']
                angle=self.Element[idx].angle
                
                return srel+Ref['ref'].len(angle)

    def positionCenter(self,ele): # Centre of the element
        return self.position(ele)+ele.sRef+ele.getLength()*0.5


    def append(self, ele, sRef=0,Ref='absolute'):  
        if isinstance(ele,list):   # a quick waz to add a series of elements if thez are givien in a list
            sRef=0
            Ref='relative'
            for p in ele:
                self.Element.append(p)
                reference={'pos':sRef,'ref':Ref}
                self.Ref.update({p:reference})
        else:                              # otherwise a single element is added        
            self.Element.append(ele)
            reference={'pos':sRef,'ref':Ref}
            self.Ref.update({ele:reference})
        

    def flatten(self,name,line,eleDB,secDB,corrDB):   # this routine is called in the facilitycontainer class to remove all arbitrary nesting of containers
        if self.isFlat>0:             # check whether a given line has been already flatten (needed for phase 2 lines
            if (len(self.Name)==7):
                line.append(self)
            return

        self.Name=name+self.Name;


        if (len(self.Name)==7):          # chgeck wether section length has 7 letters
            for ele in self.Element:
                ele.initiate(self.Name)  # initiate all elements
                eleDB[ele.Name]=ele
                try:
                    if ele.Type=='Corrector':
                        #if ele.__dict__.has_key('cor') or ele.__dict__.has_key('corx'):
                        if 'cor' in ele.__dict__ or 'corx' in ele.__dict__:
                            eleDB[ele.Name.replace('COR','CRX')]=ele
												#if ele.__dict__.has_key('cory'):		
                        if 'cory' in ele.__dict__:
                            eleDB[ele.Name.replace('COR','CRY')]=ele
                except:
                    'Alignment elem??'
                if 'cor' in ele.__dict__ or 'corx' in ele.__dict__ or 'cory' in ele.__dict__:		
                #if ele.__dict__.has_key('cor') or ele.__dict__.has_key('corx') or ele.__dict__.has_key('cory'):
                    if ('MQUA' in ele.Name) or ('MBND' in ele.Name) or ('UIND' in ele.Name):  
										    #if ele.__dict__.has_key('cor') or ele.__dict__.has_key('corx'):
                        if 'cor' in ele.__dict__ or 'corx' in ele.__dict__:
                            cname=ele.Name.replace('MQUA','MCRX').replace('MBND','MCRX').replace('UIND','MCRX')
                            prop={}
                            prop['Name']=cname
                            prop['Type']='Corrector'
                            cele=Corrector(prop)
                            corrDB[cname]=cele
												#if ele.__dict__.has_key('cory'):		
                        if 'cory' in ele.__dict__:
                            cname=ele.Name.replace('MQUA','MCRY').replace('MBND','MCRY').replace('UIND','MCRY')
                            prop={}
                            prop['Name']=cname
                            prop['Type']='Corrector'
                            cele=Corrector(prop)
                            corrDB[cname]=cele
                        
            line.append(self)            # add section to beamline
            secDB[self.Name]=self # Revive sectionDB...09.06.2015 Masamitsu
            self.isFlat=1
        else:
            for ele in self.Element:
                ele.flatten(self.Name,line,eleDB,secDB,corrDB);


    def initiate(self,name=''):

        self.Name=name+self.Name
        self.output=1
        for ele in self.Element:
            ele.initiate(self.Name)

    
    def setRange(self,start='start',end='end',out=0):      
  
        self.output=out     # inherit output from parent container
        
        if (start=='start'):
            start=self.Name
        if (self.Name==start): # start output if name math
            self.output=1 
                 
        hasend=0
        output=self.output
        for ele in self.Element:
            if isinstance(ele,LineContainer):
                outchild=ele.setRange(start,end,output)                
                #if outchild<>0:        # Not valid in python3.x
                if outchild!=0:        # if child is part of the range - parent has output
                    self.output=1
                    output=1                
                if outchild==-1:       # if child was last then
                    output=0           # suppress output for following children
                    hasend=1           # indicate parent that this is the last element

        if (self.Name==end) or (hasend==1):
            return -1            
        return self.output            



    #def writeLattice(self,CI,EM,app):
    # Change the order of input arguments and set defaults
    def writeLattice(self,app,EM=None):

        if self.output==0:
            return 0

        seq=[]          
        Last=0


        if (len(self.Name)==7): 
            for ele in self.Element:
                ds=self.position(ele)-Last
                app.writeDrift(ds)
                name=ele.writeElement(app,EM)
                Last=self.position(ele)+ele.getResLength()
                #if name<>0: # not valid in python3.x
                if name!=0:
                    seq.append(ele)  
            app.writeDrift(self.getResLength()-Last)
            app.writeLine(self,seq)
            baseseq={'Name':self.Name,'L':self.getResLength()}
            return baseseq
        else:
            for ele in self.Element:
                name=ele.writeLattice(app,EM)
                # if name<>0: # not valid in python3.x
                if name!=0:
                    seq.append(name)
            if len(self.Name)>1:
                return seq
            else:
                app.writeLine(self,seq);
            return 0
           




class VariableContainer:    # used to allow varibale length in the arm of a bunch compressor, stored in a line container
    def __init__(self,L=0,Loff=0):
        self.L=L
        self.Loff=Loff        
              
    def len(self,angle):
        dL=self.L/math.cos(angle*math.asin(1)/90)-self.Loff         
        return dL    
  
    
#----------------------------
# list of basic elements such as magnets, diagnostics etc. All are based on the SimpleContainer class

class SimpleContainer:
    
    def __init__(self,prop): 
        # makes sure that common elements are defined and calculates the length and reserved length
        if not('Name' in prop): #prop.has_key('Name')):
            prop.update({'Name':''})
            
        if not('Baugruppe' in prop): #prop.has_key('Baugruppe')):
            prop.update({'Baugruppe':'none'})
 
        if not('BG-Variant' in prop): #prop.has_key('BG-Variant')):
            prop.update({'BG-Variant':'A'})
           
        if not('index' in prop): #prop.has_key('index')):
            prop.update({'index':0})
        
        if not('p0' in prop): #prop.has_key('p0')):
            prop.update({'p0':0})
     
        if not('Length' in prop): #prop.has_key('Length')):
            prop.update({'Length':0})
 
        if not('LengthRes' in prop): #prop.has_key('LengthRes')):
            prop.update({'LengthRes':0})

        if not('sRef' in prop): #prop.has_key('sRef')):
            prop.update({'sRef':-1})
            
        if not('Tilt' in prop): #prop.has_key('Tilt')):
            prop.update({'Tilt':0}) 
                  
        self.__dict__.update(prop)
        
        if self.LengthRes <=0 :                     
            self.LengthRes=self.Length
            self.sRef=0
        elif self.sRef >=0:           
            LRes=self.sRef+self.Length
            if self.LengthRes < LRes:
                self.LengthRes=LRes
        else:
            if self.LengthRes < self.Length:
                self.sRef=0
                self.LengthRes=self.Length
            else:
                self.sRef=0.5*(self.LengthRes-self.Length)


    def __getitem__(self,field):
        exec('a=self.'+field)
        # fine for python2 and python3
        return locals()['a']
        
    def getResLength(self):
        # return the reserved length for that element
        return self.LengthRes
    
    def getLength(self):
        # return the length for that element
        return self.Length
    
    def writeElement(self,app,EM):
        return 0

        
    def initiate(self,name=""):
        # defined element name and local energy and the element entrance     
        self.Name="%s.%s%3.3d" % (name,self.Tag,self.index)


    def register(self):    
        return 
        
#---------------------------------------
# Base Elements

class Magnet(SimpleContainer):
    def __init__(self,prop):
        SimpleContainer.__init__(self, prop)
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'Magnets'})



class Dipole(Magnet):   
    def __init__(self, prop):
        Magnet.__init__(self,prop)
        if not('angle' in prop): #prop.has_key('angle')):
            self.__dict__.update({'angle':0})
        if not('design_angle' in prop): #prop.has_key('design_angle')):
            self.__dict__.update({'design_angle':0})
        if not('e1' in prop): #prop.has_key('e1')):
            self.__dict__.update({'e1':0})
        if not('e2' in prop): #prop.has_key('e2')):
            self.__dict__.update({'e2':0})
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'MBND'})
        

    def getResLength(self):
        if (self.angle==0):
            return self.LengthRes
        else:
            return self.LengthRes-self.Length+self.getLength()
   
    def getLength(self):    
        if (self.angle==0):
            return self.Length
        else:
            angrad=math.asin(1)*self.angle/90.
            if self.e1==0 or self.e2==0:
                Lpath=self.Length/math.sin(angrad)*angrad
            else:
                Lpath=0.5*self.Length/math.sin(angrad*0.5)*angrad
            return Lpath
              
    #def register(self,CI=None):
    #    if CI:
    #        CI.registerChannel(self.Name,'angle')
    #        if self.__dict__.has_key('cor'):
    #            CI.registerChannel(self.Name,'cor')

    def writeElement(self,app,EM):
        #if CI:
        #    self.angle=CI.getValue(self.Name,'angle')
        #    if self.__dict__.has_key('cor'):
        #        self.cor=CI.getValue(self.Name,'cor')
        if EM:
            EA=EM.EnergyAt(self)
            self.p0=EA[0] # Beam energy may be needed for some purpose, namely CSR
        app.writeBend(self)
        return self.Name


    
class Quadrupole(Magnet):   
    def __init__(self, prop):
        Magnet.__init__(self,prop)
        if not('k1' in prop): #prop.has_key('k1')):
            self.__dict__.update({'k1':0})
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'MQUA'})
            

    def writeElement(self,app,EM):
        # MADExclude flag becomes 'Overlap' flag, which is more general name and may be used in other applications
        #if (app.isType('madx')):
        #    if self.__dict__.has_key('MADExclude'):
        #        if self.MADExclude==1:
        #            return 0

        #if CI:
        #    self.k1=CI.getValue(self.Name,'k1')
        #    if self.__dict__.has_key('corx'):
        #        self.corx=CI.getValue(self.Name,'corx')
        #    if self.__dict__.has_key('cory'):
        #        self.cory=CI.getValue(self.Name,'cory')
        app.writeQuadrupole(self)
        return self.Name


    #def register(self,CI=None):
    #    if CI:
    #        CI.registerChannel(self.Name,'k1')
    #        if self.__dict__.has_key('corx'):
    #            CI.registerChannel(self.Name,'corx')
    #        if self.__dict__.has_key('cory'):
    #            CI.registerChannel(self.Name,'cory')



class Sextupole(Magnet):   
    def __init__(self, prop):
        Magnet.__init__(self,prop)
        if not('k2' in prop): #prop.has_key('k2')):
            self.__dict__.update({'k2':0})
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'MSEX'})    
            
    #def register(self,CI=None):                             
    #    if CI:
    #        CI.registerChannel(self.Name,'k2')
        
    def writeElement(self,app,EM):
        #if CI:
        #    self.k2=CI.getValue(self.Name,'k2')
        app.writeSextupole(self)
        return self.Name


class Solenoid(Magnet):   
    def __init__(self, prop):
        Magnet.__init__(self,prop)
        if not('ks' in prop): #prop.has_key('ks')):
            self.__dict__.update({'ks':0})
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'MSOL'})    
            
    def writeElement(self,app,EM):
        #if CI:
        #    self.ks=CI.getValue(self.Name,'ks')
        app.writeSolenoid(self)
        return self.Name

    #def register(self,CI=None):
    #    if CI:
    #        CI.registerChannel(self.Name,'ks')

            
class Corrector(Magnet):
    def __init__(self, prop):
        Magnet.__init__(self,prop)
        

    #def register(self,CI=None):
    #    if CI:
    #        if self.__dict__.has_key('corx'):
    #            CI.registerChannel(self.Name,'corx')
    #        if self.__dict__.has_key('cory'):
    #            CI.registerChannel(self.Name,'cory')

    def writeElement(self,app,EM):
        #if CI:
        #    if self.__dict__.has_key('corx'):
        #        CI.getValue(self.Name,'corx')
        #    if self.__dict__.has_key('cory'):
        #        CI.getValue(self.Name,'cory')
        app.writeCorrector(self)
        return self.Name
                       
          
# -------                   

class Vacuum(SimpleContainer):
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'Vacuum'})
        self.__dict__.update({'angle':0})

    #def register(self,CI=None):
    #    return
          
    def writeElement(self,app,EM):
        app.writeVacuum(self)
        return self.Name





#  -------                   

class Undulator(SimpleContainer):
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'ID'})   
        if not('K' in prop): #prop.has_key('K')):
            self.__dict__.update({'K':0}) 
        if not('ku' in prop): #prop.has_key('ku')):
            self.__dict__.update({'ku':0})   
        if not('kx' in prop): #prop.has_key('kx')):
            self.__dict__.update({'kx':0})           
        if not('ky' in prop): #prop.has_key('ky')):
            self.__dict__.update({'ky':1})     
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'UIND'})  
        if not('Power' in prop): #prop.has_key('Power')):
            self.__dict__.update({'Power':0})           
        if not('Waist' in prop): #prop.has_key('Waist')):
            self.__dict__.update({'Waist':1})  
            
 
    #def register(self,CI=None):
    #    if CI:
    #        CI.registerChannel(self.Name,'K')

    
    def writeElement(self,app,EM):
        #if CI:
        #    self.K=CI.getValue(self.Name,'K')
        if EM:
            EA=EM.EnergyAt(self)
            self.p0=EA[0]
            self.dP=EA[1]
        else:
            self.p0=1e9
        app.writeUndulator(self)
        return self.Name


    
# --------------
            
class RF(SimpleContainer):   
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Gradient' in prop): #prop.has_key('Gradient')):
            self.__dict__.update({'Gradient':0})
        if not('Phase' in prop): #prop.has_key('Phase')):
            self.__dict__.update({'Phase':0})
        if not('Band' in prop): #prop.has_key('Band')):
            self.__dict__.update({'Band':'S'})    
        if not('Frequency' in prop): #prop.has_key('Frequency')):
            f0=5.712e9
            if 'S' in self.Band:
                f0=2.9988e9
            if 'X' in self.Band:
                f0=11.9952e9    
            self.__dict__.update({'Frequency':f0})       
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'RACC'})  
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'RF'})
   
        # Enable flag for TDS as well as in the diagnostics
        if 'TDS' in prop['Baugruppe']:
            self.__dict__.update({'enable':0})

    def writeElement(self,app,EM):
        #if CI:
        #    self.Gradient=CI.getValue(self.Name,'Gradient')
        #    self.Phase=CI.getValue(self.Name,'Phase')
        if EM:
            EA=EM.EnergyAt(self)
            self.p0=EA[0]
            self.dP=EA[1]
        else:
            self.p0=1e9 # set it to 1 GeV if EM is not available...
        app.writeRF(self)
        # Energy manager is in charge of this...
        #if self.Tag=='RACC':
        #    if EM:
        #        EM.update(self.p0+self.Gradient/511000*math.sin(self.Phase*math.asin(1)/90)*self.Length)
        return self.Name

         
    #def register(self,CI=None):
    #    if CI:
    #        CI.registerChannel(self.Name,'Gradient')
    #        CI.registerChannel(self.Name,'Phase')

# -----                   

class Diagnostic(SimpleContainer):   
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Seval' in prop): #prop.has_key('Seval')):
            self.__dict__.update({'Seval':-1})
        if not('Cx' in prop): #prop.has_key('Cx')):
            self.__dict__.update({'Cx':1})
        if not('Cy' in prop): #prop.has_key('Cy')):
            self.__dict__.update({'Cy':1})
        if not('Cz' in prop): #prop.has_key('Cz')):
            self.__dict__.update({'Cz':0})
        if not('Sx' in prop): #prop.has_key('Sx')):
            self.__dict__.update({'Sx':0})
        if not('Sy' in prop): #prop.has_key('Sy')):
            self.__dict__.update({'Sy':0})
        if not('Sz' in prop): #prop.has_key('Sz')):
            self.__dict__.update({'Sz':0})
        if not('Distribution' in prop): #prop.has_key('Distribution')):
            self.__dict__.update({'Distribution':0})
        if not('Tag' in prop): #prop.has_key('Tag')):
            self.__dict__.update({'Tag':'DBPM'})
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'Diagnostics'})
        if not('enable' in prop): #prop.has_key('enable')):
            self.__dict__.update({'enable':0})
            
                           
    def writeElement(self,app,EM):
        app.writeDiagnostic(self)
        return self.Name 
            
    #def register(self,CI=None):  
    #    if CI:
    #        if self.__dict__.has_key('Cx'):
    #            CI.registerChannel(self.Name,'Cx')
    #        if self.__dict__.has_key('Cy'):
    #            CI.registerChannel(self.Name,'Cy')
    #        if self.__dict__.has_key('Cz'):
    #            CI.registerChannel(self.Name,'Cz')
    #        if self.__dict__.has_key('Sx'):
    #            CI.registerChannel(self.Name,'Sx')
    #        if self.__dict__.has_key('Sy'):
    #            CI.registerChannel(self.Name,'Sy')
    #        if self.__dict__.has_key('Sz'):
    #            CI.registerChannel(self.Name,'Sz')


class Photonic(Diagnostic):   
    def __init__(self, prop):
        prop.update({'Group':'Photonics'})
        if not('Tag' in prop): #prop.has_key('Tag')):    
            prop.update({'Tag':'PPRM'})
        Diagnostic.__init__(self,prop)
        
        
        
class Alignment(SimpleContainer):
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'Alignment'})  
    
    def writeElement(self,app,EM):
        app.writeAlignment(self)
        return self.Name

    def getLength(self):
        # return the reserved length for that element
        return 0
                    
        
    #def initiate(self,dummy):
    #    return
    

class Marker(SimpleContainer):
    def __init__(self, prop):
        SimpleContainer.__init__(self,prop)
        if not('Group' in prop): #prop.has_key('Group')):
            self.__dict__.update({'Group':'Marker'})  
 
    def writeElement(self,app,EM):
        app.writeMarker(self)
        return self.Name

    def getLength(self):
        # return the reserved length for that element
        return 0
                    
        
    #def register(self,CI=None):
    #    return
          
#  -------                   
