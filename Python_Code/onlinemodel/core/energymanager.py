import math
import os
import sys
from onlinemodel.core import ApplicationTemplate
import numpy as np
from string import *


from os import system

#--------
# Energy Manager Interface

class EnergyManager(ApplicationTemplate):    
    def __init__(self):
        ApplicationTemplate.__init__(self)
        self.p0=0
        self.p=0
        self.Energy={}


    def update(self,p0):
        self.p0=p0
    

    def writeLine(self,line,seq):
#        print(line.Name)
        'Nothing to do?'


    def register(self,ele,Egain=0.0):
        if (self.MapIndx!=self.MapIndxSave): # New line comes
            self.p=0
            self.MapIndxSave=self.MapIndx 
        self.Energy[ele.Name]=[self.p,Egain]
        if ('cor' in ele.__dict__) or ('corx' in ele.__dict__):
            name=ele.Name.replace('MQUA','MCRX').replace('MBND','MCRX').replace('UIND','MCRX').replace('MCOR','MCRX')
            self.Energy[name]=[self.p,Egain]
        if 'cory' in ele.__dict__:
            name=ele.Name.replace('MQUA','MCRY').replace('MBND','MCRY').replace('UIND','MCRY').replace('MCOR','MCRY')
            self.Energy[name]=[self.p,Egain]

            # Double registration for later convenience
            # ... 07.08.2015 Removed. After gaining some experience, it turns out inconvenient rather than useful.
            #self.Energy[EpicsName]=[self.p,Egain]

        return


    def demandMapID(self):
        'Energy manager always requests Map ID to Layout manger'
        return 1


    def writeVacuum(self,ele):
        self.writeMarker(ele)
        return

    def writeAlignment(self,ele):
        self.writeMarker(ele)
        return


    def writeBend(self,ele):
        # Here, it is possible to implement Egain (loss) from CSR
        # EgainCSR=***
        # self.Energy[ele.Name]=[self.p,EgainCSR]
        self.register(ele)

    def writeQuadrupole(self,ele):
        self.register(ele)
   
    def writeCorrector(self,ele):   
        self.register(ele)

    def writeSextupole(self,ele):  
        self.register(ele)
 
    def writeRF(self,ele):
        if (self.MapIndx!=self.MapIndxSave): # New line comes
            self.p=0
            self.MapIndxSave=self.MapIndx
        if ('Gradient' in ele.__dict__) and ('Phase' in ele.__dict__):   # needs a check for TDS to not give energy gain
            if 'TDS' in ele.Name:
                Egain=0.0
            else:
                E=ele.Gradient
                V=E*ele.Length
                phase=ele.Phase*math.pi/180.0
                Egain=V*math.sin(phase)
#                print(ele.Name, ' - Energy Gain:',Egain*1e-6,ele.Gradient*1e-6,self.p*1e-6) 
        else:
            Egain=0.0
        self.Energy[ele.Name]=[self.p,Egain,ele.Length]
        self.p=self.p+Egain

    def writeUndulator(self,ele): 
        self.register(ele)

    def writeDiagnostic(self,ele):
        self.register(ele)

    def writeSolenoid(self,ele):
        self.register(ele)

    def writeMarker(self,ele):
        self.register(ele)


    def demandMapID(self):
        'Energy manager always requests Map ID to Layout manger'
        return 1


  
