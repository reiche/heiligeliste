# SwissFEL Holy List

## Official Release

### Machine Lattice

- Current: [10.5.3](Release/HolyList-Lat10.5.3-Phase-Current.xlsx)

- Planned: [10.5.3](Release/HolyList-Lat10.5.3-Phase-Planned.xlsx)

- Final: [10.5.4](Release/HolyList-Lat10.5.4-Phase-Final.xlsx)


_(Released: 13/03/2025 - 15:03:40)_ 

### Optical Beamlines

- Aramis - not released yet

- Athos - not released yet

## The Holy List Git Repository

The git repository is the official location to access the current (and former) Holy Lists as well as all files needed to generate the lists locally. There will be also an official instance of the git repository on AFS, located at:

`/afs/psi.ch/project/SwissFEL/HeiligeListe`
 
 as an alternative to the github website.

 The repositories contains several files and folders, which are briefly described in the following:

 - **Python_Code**: A directory containing all python code to generate the holy list, compare lists and other activities. This is explained in more detail in the next sections. The responsible person for its content is Sven Reiche
 - **Release**: A directory with the official release of all phases (current,  planned and future)
 - **Sub_assemblies_Lists**: a directory with the component lists and all assembly group information. It also holds the documentation on the process of generating the holy lists. These files are maintained by Lother Schulz, Silke Pfinninger and Dominique Hauenstein
 - **HeiligeListe.py**: The main program to generate, comapre or release the holy lists. 
 - **README.md**: The source file to generate the front page of the git repository.

 ### Generating a Holy List

Note that the underlying scripting language is Python for generating the holylist. It requires some additional packages, most important numpy, pandas and xlsxwriter. 

 #### Step 1 - Init or Update the Git Resository

 The initial step is to transfer files from the git ripository to your local hard drive. If it is the first you have to clone the git repository, after that just an update is sufficient:

 ##### Cloning the Git Repository:

 Go into the direcotory, where you want to store the local copy of the git repository (e.g. Desktop).

 Type the command: 
 
 `git clone https://gitlab.psi.ch/reiche/heiligeliste.git`

 This should generate a folder with the name heiligeliste. 

##### Updating the Git Repository

In the case a new version has officially released, one should update also the local directory. For that enter the directory and type 

`git pull origin`

This replaces all older files in the local directory with the newer ones from the git repository.



#### Step 2 - Initializing a Working Directory
The files, which are in the git directory shouldn't be edited. It is better to copy them into a local directory. This is done by the command 

`python HeiligeListe.py Init -d Work -p Current`

The keyword `Init` indicates the process of generating a local copy of the files needed to generate a Heilige Liste. The argument after the `-d` flag indicates the name of the working directory, which will be generated and all essential files copied into it. If the directory exists then it will be deleted first. The flag `-p` indicates the phase. Allows values are `Current`, `Planned`, `Final`, and `All`. Note that all files in the working directory are not tracked by git. For published changes a different command is needed.  The order in which working directory and phase are defined is arbitray. they can be even omitted falling back to the default values of `Work` for the working directory and `Current` for the phase.

There is an alias `HLInit` which combines the syntax `python HeiligeListe.py Init` for a more compact command.


#### Step 3 - Generating the Holy List
The commando to generate a new holy list is:

`python HeiligeListe.py Generate -d Work -p Current`

Meaning of working directory and phase is the same as for the previous commando, including the default values.
If the working directory is not found then the `Init` instruction is executed. In the root level of the working directory the Excel File of the holy list will be found.

The alias is `HLGenerate`

#### Step 4 - Publishing and Releasing the Holy List

The command for publishing and releasing a new holy list is:

`python HeiligeListe.py Release -d Work -p Current`. 

It copies all files in the working directory, which were copied in the initialization step and then modified afterwards, back to the Sub_assemles_List directory as well as the Excel File of the holy list into the Release folder. The existing excel file will be moved into a backup folder. Then the changes are commited to git and pushed to the gitlab site. It requests username and password (as any publishing on gitlab) in the process to process. Note that the publishing is only allowed for people who are part of the Holy List Project on Gitlab

The alias is `HLRelease`

#### Step 5 - Extracting Subsections of the Holy List

The commando is similar to generating a holy list with the additional information on the filter. The syntax is

`python HeiligeListe.py Extract -d Work -p Current -f SINSB01`

The new flag is `-f` defining a regualr expression filter. E.g. the value above only writes out the section SINSB01, while SATSY0[23] writes out SATSY02 and SATSY03. The generated excel file has only one tab and is inteded to be used as an import for 3DXperience.

the alias is `HLExtract`